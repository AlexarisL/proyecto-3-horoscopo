program Horoscopo;

uses
  crt;

var
  nombre, signo, pre: string;
  dia, mes: integer;
  n1, n2, op: integer;
  Predicciones: array [0..11, 0..4] of string;

procedure menu;
begin
  writeln('  _____________________________________');
  writeln('*                Menu                   *');
  writeln('*                                       *');
  writeln('* 1. Registro de Usuario                *');
  writeln('*                                       *');
  writeln('* 2. Lectura de Horoscopo               *');
  writeln('*                                       *');
  writeln('* 3. Salir del programa                 *');
  writeln('*                                       *');
  writeln(' ______________________________________');
  writeln;
end;

procedure Mostrar_Signo;
begin
  if ((dia >= 21) and (mes = 3)) or ((dia <= 20) and (mes = 4)) then
  begin
    signo := 'Aries';
    n1 := 0;
  end
  else if ((dia >= 24) and (mes = 9)) or ((dia <= 23) and (mes = 10)) then
  begin
    signo := 'Libra';
    n1 := 1;
  end
  else if ((dia >= 21) and (mes = 4)) or ((dia <= 21) and (mes = 5)) then
  begin
    signo := 'Tauro';
    n1 := 2;
  end
  else if ((dia >= 24) and (mes = 10)) or ((dia <= 22) and (mes = 11)) then
  begin
    signo := 'Escorpio';
    n1 := 3;
  end
  else if ((dia >= 22) and (mes = 5)) or ((dia <= 21) and (mes = 6)) then
  begin
    signo := 'Geminis';
    n1 := 4;
  end
  else if ((dia >= 23) and (mes = 11)) or ((dia <= 21) and (mes = 12)) then
  begin
    signo := 'Sagitario';
    n1 := 5;
  end
  else if ((dia >= 22) and (mes = 6)) or ((dia <= 22) and (mes = 7)) then
  begin
    signo := 'Cancer';
    n1 := 6;
  end
  else if ((dia >= 22) and (mes = 12)) or ((dia <= 20) and (mes = 1)) then
  begin
    signo := 'Capricornio';
    n1 := 7;
  end
  else if ((dia >= 23) and (mes = 7)) or ((dia <= 23) and (mes = 8)) then
  begin
    signo := 'Leo';
    n1 := 8;
  end
  else if ((dia >= 21) and (mes = 1)) or ((dia <= 18) and (mes = 2)) then
  begin
    signo := 'Acuario';
    n1 := 9;
  end
  else if ((dia >= 24) and (mes = 8)) or ((dia <= 23) and (mes = 9)) then
  begin
    signo := 'Virgo';
    n1 := 10;
  end
  else if ((dia >= 19) and (mes = 2)) or ((dia <= 20) and (mes = 3)) then
  begin
    signo := 'Piscis';
    n1 := 11;
  end;
end;

procedure AsignacionPre;
begin
  Predicciones[0, 0] := 'Podras deshacerte de una pesada carga que lleva demasiado tiempo a tu lado.';
  Predicciones[0, 1] := 'Viviras la pasion mas absoluta, la sentiras por todos los poros de tu piel.';
  Predicciones[0, 2] :=
    'Ojo con perder la perspectiva real de las cosas y dejarte llevar por lo que los demas hagan o digan. Eso no te va a venir nada bien, incluso si piensas que asi te esfuerzas menos o caes mejor.';
  Predicciones[0, 3] :=
    'Algunos temas financieros lo preocuparan demasiado. Durante esta jornada, evite utilizar la terquedad y pida asesoramiento si es que lo necesita.';
  Predicciones[0, 4] :=
    'Se abre un abanico de posibilidades en tu vida sentimental, es probable que conozcas a muchas personas. Si tienes pareja, esto podria ser motivo de pleito.';

  Predicciones[1, 0] := 'Trabajo: un personaje controvertido puede hacerle perder una buena oportunidad.';
  Predicciones[1, 1] := 'Tu pareja circula entre personas atractivas; la fidelidad se convertira en reto.';
  Predicciones[1, 2] :=
    'Las actividades artisticas, en cualquiera de sus formas, te atraeran mucho y en eso vas a pasar horas, y lo cierto es que te pueden traer ratos muy confortadores ';
  Predicciones[1, 3] := 'Se reactivan problemas, pero con gran inteligencia los resuelve de modo positivo';
  Predicciones[1, 4] := 'Una persona le confia un delicado secreto, pero se desvive por contarlo. Calma';

  Predicciones[2, 0] :=
    'Cuida tu salud y si te sientas mal, no uses indiscriminadamente medicamentos, perjudicaras tu organismo.';
  Predicciones[2, 1] :=
    'Recibiras propuestas economicas de trascendencia y gracias tu intuicion realizaras inversiones que te llevaran a cierta prosperidad ';
  Predicciones[2, 2] :=
    'Deberas aclarar una situacion en casa y para evitar lastimar a tu pareja tienes que evitar decir cosas de las que no estas seguro';
  Predicciones[2, 3] := 'Retornara un viejo amor.';
  Predicciones[2, 4] := 'Puedes ser engañado, mentirse a ti mismo o actuar fraudulentamente hacia los demas';

  Predicciones[3, 0] :=
    'Dolores musculares continuos por exceso de preocupaciones y compromisos, los masajes te haran muy bien.';
  Predicciones[3, 1] :=
    'Se te presentaran oportunidades para invertir en obras de construccion, asuntos vinculados a la tierra como la agricultura, el campo o la mineria.';
  Predicciones[3, 2] := 'Una bendicion del cielo llegara a tu familia gracias a un milagro';
  Predicciones[3, 3] := 'Encontraras un amor verdadero que les traera grades momentos hermosos.';
  Predicciones[3, 4] := 'veras materializado alguno de tus deseos mas importantes.';

  Predicciones[4, 0] := 'Es el momento de volver a construir sobre las ruinas de una ciudad del pasado.';
  Predicciones[4, 1] :=
    'Se sentira apasionado, sera una jornada ideal para tener una cita a solas con esa persona que le gusta';
  Predicciones[4, 2] := 'Preparese animicamente, ya que se encontrara muy susceptible ante las situaciones que se le presente';
  Predicciones[4, 3] :=
    'Se presentaran oportunidades para hacer buenos negocios, es posible que te lleguen propuestas laborales';
  Predicciones[4, 4] :=
    'Si tienes temas legales podrian resolverse en este mes, tambien podras hacer buenos acuerdos con los asuntos que tengas pendientes. ';

  Predicciones[5, 0] := 'Llegara gente con tendencia a lo fantasioso; cuide sus intereses y no haga cambios';
  Predicciones[5, 1] := 'La ternura y la sensibilidad pareceran estar ausentes. Tome otra actitud o su pareja querra alejarse.';
  Predicciones[5, 2] := 'Un asunto que le quita el sueño dejara de ser un problema.';
  Predicciones[5, 3] := 'Nuevos horizontes. Un encuentro fortuito reavivara la seduccion';
  Predicciones[5, 4] :=
    'Si esta pensando en comprarse ese electrodomestico que tanto desea para su hogar, no lo dude y dese el gusto';

  Predicciones[6, 0] :=
    'Hoy es muy probable que recibas ayuda, apoyo y consuelo de alguien muy querido. Puede ser de tu verdadera madre o de una persona muy maternal, como una madre adoptiva.';
  Predicciones[6, 1] :=
    'Preparate para recibir noticias agradables y amor? Podrian ser cartas, llamadas telefonicas o postales. Pasaras largo rato en el telefono poniendote al dia con amigos';
  Predicciones[6, 2] :=
    'Un cambio repentino e inesperado para mejor ocurrira con respecto a tu situacion financiera. Quizas descubras una libreta de ahorro de una cuenta vieja de la que no te acordabas';
  Predicciones[6, 3] :=
    'Los proyectos estaran favorecidos. El trabajo en equipo, los vinculos con colegas y amigos, una estrategia que involucre mucha accion, pasion, entusiasmo, organizacion y responsabilidad';
  Predicciones[6, 4] :=
    'Sorpresas en el amor, romances a primera vista, un embarazo. Algunos se casaran inesperadamente. Conversa con tu corazon, escuchalo sin temores, no permitas que el pasado dirija tu futuro, haz el esfuerzo de pensar y disfrutar el momento.';

  Predicciones[7, 0] :=
    'Dificultades pasajeras en casa podrian hacerte desear quedarte para resolverlas. Sin embargo, puede haber otras responsabilidades ejerciendo presion, lo que te colocara en un dilema.';
  Predicciones[7, 1] :=
    'Hoy podrias dedicar tu tiempo a divertirte en familia. Quizas decidas hacer un viajecito con ellos. Disfrutaras mucho si sales de la ciudad y visitas un lugar distinto.';
  Predicciones[7, 2] :=
    'Tu entusiasmo e imaginacion seran muy fuertes hoy, asi que podras vender tus servicios o ideas. Si posees tu propio negocio, es un gran dia para atraer nuevos clientes o armar una colaboracion con tus colegas.';
  Predicciones[7, 3] :=
    'te importa mucho la apariencia fisica, debes de cuidar tu aspecto fisico de forma saludable y natural, evita metodos que dañen tu salud. Toma mucha agua y cuida tus riñones.';
  Predicciones[7, 4] :=
    'Buena alianza y sociedad te mantendran ocupado casi todo el año, piensa antes de decidir. Utiliza mucho tu intuicion, tu mejor carta es la reflexion y el analisis.';

  Predicciones[8, 0] :=
    'Eres indulgente por naturaleza. Eres conciente de que nadie es perfecto. Si alguien hiere tus sentimientos, tu intentas ignorarlo. Pero tu generosidad puede llegar a veces a ser demasiado extrema.';
  Predicciones[8, 1] :=
    'Tu suministro de energia parecera ilimitado, y deberas obtener muchos logros sin luego sentirte vacio. Tal vez te sientas con ganas de realizar un ejercicio saludable que te haga sudar.';
  Predicciones[8, 2] :=
    'Ahora necesitaras desarrollar mas la confianza en los demas. Podras sentirte un poco frustrado ya que quieres hacer las cosas a tu modo. Pero encontraras limitaciones. Tus sueños necesitaran mas tiempo, dinero y energia ';
  Predicciones[8, 3] :=
    'Camina con mas energia. Puedes sentirte comoda a ritmo lento y seguro pero recuerda que las situaciones del dia requieren algo mucho mas dinamico y fuerte. Toma tu vestimenta de lider y lucela con orgullo.';
  Predicciones[8, 4] :=
    'Tu deseada evolucion laboral podria hacerse realidad, pero debes estar muy atento en el trabajo y esforzarte en ser el mejor para alcanzar la excelencia.';

  Predicciones[9, 0] :=
    'Hoy es un dia fantastico para ti. Descubriras que la niebla que ha estado nublando tu mente en estos ultimos dias se ha disipado finalmente, y que estas lista para entrar en accion. La comunicacion juega un rol importante en tu forma de proceder.';
  Predicciones[9, 1] :=
    'Habra un sentimiento de expansion y creatividad hoy. Tu mente estara llena de visiones positivas sobre el futuro. Podrias tener nuevas ideas sobre una nueva estrategia para tu carrera o un nuevo enfoque en una relacion personal.';
  Predicciones[9, 2] :=
    'No tomes por sentado nada sobre las personas hoy, porque te saldra el tiro por la culata. Si piensas que alguien comprendio lo que acabas de decir, ¡asegurate dos veces! Estaras operando en un nivel hoy, atrapado en tus propios pensamientos.';
  Predicciones[9, 3] :=
    'Un poco de ejercicio fisico, ya sea ir al gimnasio, nadar o, simplemente, pasear, te vendria muy bien para relajarte. Las dietas sin orientacion medica traen efectos negativos a tu organismo. Si tienes problemas para dormir, trata de relajarte ';
  Predicciones[9, 4] :=
    'Imprevistos economicos. Cuidate de estafadores. Resuelves un problema que te preocupa. Ahorra. Se te presenta una oportunidad para emprender un negocio propio, debes analizar el mercado y tomar la mejor decision.';

  Predicciones[10, 0] :=
    'Puedes expresarte de manera muy poderosa. Tienes una excitante manera de hablar. Cuando un tema te apasiona, atraes a una gran audiencia. A la gente le fascina lo que dices. Hoy atraeras atencion al expresar tu punto de vista sobre algo.';
  Predicciones[10, 1] :=
    'Un amigo intimo o una pareja no te contestara tus llamados, y tus inseguridades te surgiran haciendote pensar que tu amigo ya no esta interesado en ti. No caigas en esta trampa. Posiblemente este rodeado de personas demandantes';
  Predicciones[10, 2] :=
    'Este es un buen dia para expresar lo que piensas a tus superiores. Puede que tengas ganas de hacer alguna sugerencia acerca de asuntos relacionados con los empleados. Tal vez tengas un buen plan para dividir los equipos de manera diferente.';
  Predicciones[10, 3] := 'Comenzaras a ver cambios en algo relacionado con tu trabajo.';
  Predicciones[10, 4] :=
    'Los casados deben dejarse llevar por la espontaneidad. Los solteros estaran muy reflexivos, metidos en sus pensamientos. Tomate el tiempo necesario para renovarte. Este periodo esta favorablemente condicionado para disfrutar del amor.';

  Predicciones[11, 0] :=
    'Unas vacaciones largamente esperadas podrian estar por llegar. Hoy en tu agenda debe figurar el estudio de libros de viajes. Quizas pases gran parte del dia realizando los arreglos necesarios';
  Predicciones[11, 1] :=
    'Una visitante, que tiene algunos problemas, ira a tu casa en busca de consejo y comprension. Puede traerte alguna otra noticia que te producira un shock. Una crisis laboral etsa por pasar';
  Predicciones[11, 2] :=
    'Tu determinacion de triunfar te llevara lejos, a pesar de que los acontecimientos se muevan mas lentamente de lo que quisieras. Haras buenos contactos iniciales con clientes potenciales que te recompensaran mas adelante.';
  Predicciones[11, 3] :=
    'Cuida tu piel, ve al dermatologo estaras preocupado por afecciones a la piel. Debes de escapar de la monotonia. Segun la persona, de sus valores o de su ideologia, los contactos espirituales atraeran bienestar psiquico y fisico.';
  Predicciones[11, 4] :=
    'El 2022 sera un excelente año para independizarte. Apertura de negocio propio es algo que hace mucho ya te habias planteado y este es un buen momento para concretar todo proyecto. ';
end;

procedure listaOp;
begin
  repeat
    clrscr;
    menu;
    writeln('---------------------');
    writeln('Seleccione una opcion:');
    writeln('---------------------');
    readln(op);
    clrscr;
    case op of
      1:
      begin
        writeln('Bienvenido, por favor ingrese sus datos');
        writeln('igrese su Nombre:');
        writeln('Ej: "Maria Perez"');
        readln(nombre);
        writeln;
        writeln('Ingrese su fecha de nacimiento');
        writeln;
        writeln('Ingrese el dia');
        readln(dia);
        writeln;
        writeln('Ingrese el mes');
        writeln;
        writeln('Ej: 7');
        readln(mes);
        writeln('Hola ', nombre, ' tu fecha de naciemiento es: ', dia, '/', mes);
        writeln;
        writeln('presione ENTER para volver al menu');
        readln;
      end;
      2: if (Nombre <> '') then
        begin
          Randomize;
          Mostrar_Signo();
          writeln('Hola ', Nombre, ' tu sino es: ', Signo);
          writeln;
          n2 := Random(5) - 1;
          pre := Predicciones[n1, n2];
          writeln('Y tu prediccion es: ', pre);
          readln;
          Nombre := '';
        end
        else
        begin
          writeln('Debes primero registrarte para que podamos darte una Prediccion');
          writeln;
          writeln('PRESIONE ENTER PARA CONTINUAR');
          readln();
        end;
      3:
      begin
        writeln('Que la Suerte este contigo');
        readln;
        writeln('Vuelve pronto');
      end;
    end;
  until (op = 3);
end;

begin
  AsignacionPre;
  listaOp;
end.
